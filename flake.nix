{
  description = "Rust scratch - REPL setup for Rust.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: utils.lib.simpleFlake {
    inherit self nixpkgs;
    name = "pidgeon";
    config = { allowUnfree = true; };
    shell = { pkgs }: pkgs.mkShell {
      packages = with pkgs; 
      [
        # Nix
        nil
        nixpkgs-fmt

        # Rust
        llvmPackages.clangNoLibcxx
        llvmPackages.lldb
        rustc
        cargo
        clippy
        rustfmt
        rust-analyzer
        cargo-edit
        pkg-config
        openssl
        sqlx-cli
        evcxr

        # Shell
        nodePackages.bash-language-server
        shfmt
        shellcheck

        # Misc
        nodePackages.prettier
        nodePackages.yaml-language-server
        marksman
        taplo
      ];
    };
  };
}
