use futures::{FutureExt, StreamExt};

#[tokio::main]
async fn main() {
  let (sender, receiver) = flume::unbounded();

  let generator_sender = sender.clone();
  let generator_handle = tokio::spawn(async move {
    for i in 0..100 {
      generator_sender.send_async(i).await.unwrap();
      println!("Sent {}", i);
      tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
    }
  });

  let mut stream = receiver.into_stream();
  let receiver_handle = tokio::spawn(async move {
    loop {
      loop {
        let i = match stream.next().now_or_never().flatten() {
          Some(i) => i,
          None => break,
        };
        println!("Received {}", i);
      }

      tokio::time::sleep(tokio::time::Duration::from_secs(10)).await;
    }
  });

  generator_handle.await.unwrap();
  receiver_handle.await.unwrap();
}
